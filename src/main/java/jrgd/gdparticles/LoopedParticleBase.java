package jrgd.gdparticles;

import jrgd.particlelib.Types.LoopedParticleEffect;
import jrgd.particlelib.Types.Vector3d;
import net.minecraft.particle.ParticleEffect;

public class LoopedParticleBase {
    String name = null;
    LoopedParticleEffect base = null;
    ParticleEffect particle = null;
    Vector3d offset = new Vector3d(0,0,0);
    int delayTicks = 1;
    int frameOffset = 0;
    boolean reverse = false;

    public LoopedParticleEffect quickRender() {
        return base;
    }
}
