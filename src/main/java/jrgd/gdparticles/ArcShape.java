package jrgd.gdparticles;

import jrgd.particlelib.Types.Vector3d;

import java.util.ArrayList;

public class ArcShape {
    private double degrees;
    private int points;
    public ArcShape(Double Degrees, int Points) {
        if (Degrees < -360)
            this.degrees = -360;
        else if (Degrees > 360)
            this.degrees = 360;
        else
            this.degrees = Degrees;
        this.points = Points;
    }

    public Shape build() {
        ArrayList<Vector3d> Points = new ArrayList<>();
        for (int point = 0; point < this.points; point++) {
            Vector3d position = new Vector3d(0,0,0);
            position.x = Math.cos(Math.toRadians((point / (double) (this.points - 1)) * this.degrees));
            position.y = Math.sin(Math.toRadians((point / (double) (this.points - 1)) * this.degrees));
            Points.add(position);
        }
        return new Shape(Points);
    }
}
