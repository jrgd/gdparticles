package jrgd.gdparticles;

public class InterpolationMode {
    private boolean smooth;
    private int subdiv;

    public InterpolationMode(boolean Smooth, int Subdivisions) {
        this.smooth = Smooth;
        this.subdiv = Subdivisions;
    }

    public boolean isSmooth() {
        return this.smooth;
    }

    public int Subdivisions() {
        return this.subdiv;
    }
}
