package jrgd.gdparticles;

public class Function {
    public static int LCM(int a, int b) {

        for (int i = 1; i <= b; i++) {
            if (i * a % b == 0)
                return java.lang.Math.abs(i * a);
        }
        return -1;
    }
}

