package jrgd.gdparticles;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import jrgd.particlelib.Types.*;
import jrgd.particlelib.Types.ParticlePrefs.DustParticlePrefs;
import net.minecraft.command.arguments.DimensionArgumentType;
import net.minecraft.command.arguments.EntityArgumentType;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.dimension.DimensionType;

import java.util.ArrayList;
import java.util.Arrays;

import static com.mojang.brigadier.arguments.StringArgumentType.string;
import static jrgd.particlelib.LoopedParticleRegistry.*;
import static net.minecraft.server.command.CommandManager.argument;

public class Commands {

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(CommandManager.literal("renderpresets")
                .requires(source -> source.hasPermissionLevel(4))
                .executes(Commands::render)
        );
        dispatcher.register(CommandManager.literal("playeffect")
                .requires(source -> source.hasPermissionLevel(4))
                .then(argument("particleEffect", string())
                .executes(Commands::play))
        );
        dispatcher.register(CommandManager.literal("killeffect")
                .requires(source -> source.hasPermissionLevel(4))
                .then(argument("Effect", string())
                        .executes(Commands::kapEffect))
        );
        dispatcher.register(CommandManager.literal("killcontext")
                .requires(source -> source.hasPermissionLevel(4))
                .then(argument("Player", EntityArgumentType.player())
                        .executes(Commands::kapPlayer))
                .then(argument("Player", EntityArgumentType.player())
                        .then(argument("Effect", string())
                                .executes(Commands::kapPlayerEffect)))
                .then(argument("World", DimensionArgumentType.dimension())
                        .executes(Commands::kapWorld))
                .then(argument("World", DimensionArgumentType.dimension())
                        .then(argument("Effect", string())
                                .executes(Commands::kapWorldEffect)))
        );
    }

    private static int render(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
        ServerPlayerEntity player = ctx.getSource().getPlayer();

        Orbiter cons = new Orbiter("vortex_0", ParticleTypes.FIREWORK, 40)
                .SetScale(.6)
                .SetOffset(new Vector3d(0,2,0));
        LoopedParticleEffect vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("vortex_1", ParticleTypes.FIREWORK, 40)
                .SetScale(.6)
                .SetOffset(new Vector3d(0,2,0))
                .SetRotation(new Vector3d(0,180,0))
                .SetFrameOffset(0);

        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("vortex_2", ParticleTypes.FIREWORK, 40)
                .SetScale(.375)
                .SetOffset(new Vector3d(0,1.4,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("vortex_3", ParticleTypes.FIREWORK, 40)
                .SetScale(.375)
                .SetOffset(new Vector3d(0,1.4,0))
                .SetRotation(new Vector3d(0,180,0))
                .SetFrameOffset(0);
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        vortex = cons.quickRender();
        AddEffect(vortex);
        cons = new Orbiter("vortex_4", ParticleTypes.FIREWORK, 40)
                .SetScale(.2)
                .SetOffset(new Vector3d(0,.8,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("vortex_5", ParticleTypes.FIREWORK, 40)
                .SetScale(.2)
                .SetOffset(new Vector3d(0,.8,0))
                .SetRotation(new Vector3d(0,180,0))
                .SetFrameOffset(0);
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        DustParticlePrefs dust = new DustParticlePrefs(0f,1f,0f, 1f);
        cons = new Orbiter("smoothTest", dust.build(), 5)
                .SetScale(3)
                .SetOffset(new Vector3d(0,3,0))
                .SetFrameOffset(0)
                .SetInterpolationMode(new InterpolationMode(true, 8));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("linearTest", ParticleTypes.FIREWORK, 3)
                .SetScale(3)
                .SetOffset(new Vector3d(0,3,0))
                .SetFrameOffset(0)
                .SetInterpolationMode(new InterpolationMode(false, 3));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("flame_0", ParticleTypes.FLAME, 60)
                .SetScale(3)
                .SetOffset(new Vector3d(0,0,0))
                .SetFrameOffset(0)
                .SetCount(0)
                .SetSpeed(1)
                .SetDirectionalOption(true)
                .SetOption(new Vector3d(-.15,0,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("flame_1", ParticleTypes.FLAME, 60)
                .SetScale(3)
                .SetOffset(new Vector3d(0,0,0))
                .SetFrameOffset(20)
                .SetCount(0)
                .SetSpeed(1)
                .SetDirectionalOption(true)
                .SetOption(new Vector3d(-.15,0,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("flame_2", ParticleTypes.FLAME, 60)
                .SetScale(3)
                .SetOffset(new Vector3d(0,0,0))
                .SetFrameOffset(40)
                .SetCount(0)
                .SetSpeed(1)
                .SetDirectionalOption(true)
                .SetOption(new Vector3d(-.15,0,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("flame_3", ParticleTypes.FLAME, 60)
                .SetScale(3)
                .SetOffset(new Vector3d(0,0,0))
                .SetFrameOffset(0)
                .SetCount(0)
                .SetSpeed(1)
                .SetReverse(true)
                .SetDirectionalOption(true)
                .SetOption(new Vector3d(-.15,0,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("flame_4", ParticleTypes.FLAME, 60)
                .SetScale(3)
                .SetOffset(new Vector3d(0,0,0))
                .SetFrameOffset(20)
                .SetCount(0)
                .SetSpeed(1)
                .SetReverse(true)
                .SetDirectionalOption(true)
                .SetOption(new Vector3d(-.15,0,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("flame_5", ParticleTypes.FLAME, 60)
                .SetScale(3)
                .SetOffset(new Vector3d(0,0,0))
                .SetFrameOffset(40)
                .SetCount(0)
                .SetSpeed(1)
                .SetReverse(true)
                .SetDirectionalOption(true)
                .SetOption(new Vector3d(-.15,0,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        cons = new Orbiter("flameBurst", ParticleTypes.FLAME, 1)
                .SetScale(0)
                .SetOffset(new Vector3d(0,0,0))
                .SetFrameOffset(0)
                .SetInterpolationMode(new InterpolationMode(true, 128))
                .SetCount(0)
                .SetSpeed(.2)
                .SetDirectionalOption(true)
                .SetOption(new Vector3d(2,0,0));
        vortex = cons.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        dust = new DustParticlePrefs(.835f,.698f,.361f, .3f);
        ShapedOrbiter shapeTest = new ShapedOrbiter("moon_0", dust.build(), 2, new ArcShape(180.0,20).build().SetScale(new Vector3d(.5,.5,.5)).SetRotation(new Vector3d(0,0,90)).SetOffset(new Vector3d(-.5, 0, 0)))
                .SetScale(.5)
                .SetOffset(new Vector3d(0,3,0))
                .SetRotation(new Vector3d(23.5,0,0))
                .SetReverse(false)
                .SetFrameOffset(0)
                .SetInterpolationMode(new InterpolationMode(true, 20));
        vortex = shapeTest.quickRender().setCustomRenderProps(400,20);
        AddEffect(vortex);
        SaveEffect(vortex);

        dust = new DustParticlePrefs(.392f,.447f,.58f, .3f);
        shapeTest = new ShapedOrbiter("moon_1", dust.build(), 2, new ArcShape(180.0,20).build().SetScale(new Vector3d(.5,.5,.5)).SetRotation(new Vector3d(0,0,90)).SetOffset(new Vector3d(-.5, 0, 0)))
                .SetScale(.5)
                .SetOffset(new Vector3d(0,3,0))
                .SetRotation(new Vector3d(23.5,0,0))
                .SetReverse(false)
                .SetFrameOffset(1)
                .SetInterpolationMode(new InterpolationMode(true, 20));
        vortex = shapeTest.quickRender().setCustomRenderProps(400,20);
        AddEffect(vortex);
        SaveEffect(vortex);

        shapeTest = new ShapedOrbiter("ring", dust.build(), 2, new ArcShape(360.0,40).build().SetScale(new Vector3d(.5,.5,.5)).SetRotation(new Vector3d(0,0,90)).SetOffset(new Vector3d(.5, 0, 0)))
                .SetScale(.5)
                .SetOffset(new Vector3d(0,2,0))
                .SetRotation(new Vector3d(0,0,0))
                .SetVerticalSine(true)
                .SetVSineHeight(1)
                .SetVSineSpeed(2)
                .SetReverse(true)
                .SetFrameOffset(1)
                .SetInterpolationMode(new InterpolationMode(true, 20));
        vortex = shapeTest.quickRender();
        AddEffect(vortex);
        SaveEffect(vortex);

        shapeTest = new ShapedOrbiter("linearShape", ParticleTypes.FLAME, 5, new ArcShape(120.0, 3).build())
                .SetScale(3)
                .SetInterpolationMode(new InterpolationMode(false, 20));
        vortex = shapeTest.quickRender().setCustomRenderProps(1,1);
        AddEffect(vortex);
        SaveEffect(vortex);

        LPEGroup vortexGroup = new LPEGroup("vortex", new ArrayList<>(Arrays.asList(GetEffect("vortex_0"),
                GetEffect("vortex_1"),
                GetEffect("vortex_2"),
                GetEffect("vortex_3"),
                GetEffect("vortex_4"),
                GetEffect("vortex_5"))));
        LPEGroup flameGroup = new LPEGroup("flame", new ArrayList<>(Arrays.asList(GetEffect("flame_0"),
                GetEffect("flame_1"),
                GetEffect("flame_2"),
                GetEffect("flame_3"),
                GetEffect("flame_4"),
                GetEffect("flame_5"))));
        LPEGroup moonGroup = new LPEGroup("moon", new ArrayList<>(Arrays.asList(GetEffect("moon_0"),
                GetEffect("moon_1"))));

        AddGroup(vortexGroup);
        SaveGroup(vortexGroup);

        AddGroup(moonGroup);
        SaveGroup(moonGroup);

        AddGroup(flameGroup);
        SaveGroup(flameGroup);

        return 1;
    }

    private static int play(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
        ServerPlayerEntity player = ctx.getSource().getPlayer();

        String effect = StringArgumentType.getString(ctx, "particleEffect");

        ArrayList<LoopedParticleEffect> group = null;
        if (GetGroup(effect) != null) {
            group = GetGroup(effect).getEffects();
        }
        LoopedParticleEffect single = GetEffect(effect);

        if (group != null) {
            for (LoopedParticleEffect p : group) {
                AddContainer(new LoopedParticleContainer(p, player));
            }
            return 1;
        }
        else if (single != null) {
            AddContainer(new LoopedParticleContainer(single, player));
            return 1;
        }
        else return -1;
    }

    private static int kapEffect(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
        String effect = StringArgumentType.getString(ctx, "Effect");

        ArrayList<LoopedParticleEffect> group = null;
        if (GetGroup(effect) != null) {
            group = GetGroup(effect).getEffects();
        }
        LoopedParticleEffect single = GetEffect(effect);

        if (group != null) {
            for (LoopedParticleEffect p : group) {
                RemoveContainer(p);
            }
            return 1;
        }
        else if (single != null) {
            RemoveContainer(single);
            return 1;
        }

        else return -1;
    }

    private static int kapPlayer(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
        ServerPlayerEntity player = EntityArgumentType.getPlayer(ctx, "Player");
        RemoveContainer(player);

        return 1;
    }

    private static int kapPlayerEffect(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
        ServerPlayerEntity player = EntityArgumentType.getPlayer(ctx, "Player");
        String effect = StringArgumentType.getString(ctx, "Effect");

        ArrayList<LoopedParticleEffect> group = null;
        if (GetGroup(effect) != null) {
            group = GetGroup(effect).getEffects();
        }
        LoopedParticleEffect single = GetEffect(effect);

        if (group != null) {
            for (LoopedParticleEffect p : group) {
                RemoveContainer(player, p);
            }
            return 1;
        }
        else if (single != null) {
            RemoveContainer(player, single);
            return 1;
        }

        else return -1;
    }

    private static int kapWorld(CommandContext<ServerCommandSource> ctx) {
        DimensionType world = DimensionArgumentType.getDimensionArgument(ctx, "World");
        RemoveContainer(world);

        return 1;
    }

    private static int kapWorldEffect(CommandContext<ServerCommandSource> ctx) {
        DimensionType world = DimensionArgumentType.getDimensionArgument(ctx, "World");
        String effect = StringArgumentType.getString(ctx, "Effect");

        ArrayList<LoopedParticleEffect> group = null;
        if (GetGroup(effect) != null) {
            group = GetGroup(effect).getEffects();
        }
        LoopedParticleEffect single = GetEffect(effect);

        if (group != null) {
            for (LoopedParticleEffect p : group) {
                RemoveContainer(world, p);
            }
            return 1;
        }
        else if (single != null) {
            RemoveContainer(world, single);
            return 1;
        }
        else return -1;
    }

}
