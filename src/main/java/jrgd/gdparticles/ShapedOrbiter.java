package jrgd.gdparticles;

import jrgd.particlelib.Types.LoopedParticleEffect;
import jrgd.particlelib.Types.ParticlePrefab;
import jrgd.particlelib.Types.Vector3d;
import net.minecraft.particle.ParticleEffect;

import java.util.ArrayList;

import static jrgd.gdparticles.Function.LCM;
import static jrgd.particlelib.Calculations.Rotation.*;
import static jrgd.particlelib.Calculations.Range.*;

public class ShapedOrbiter extends LoopedParticleBase {

    // Add interpolation, old geometric and arc-based (subdivide CircleGenerator to make subpoints)

    double circleScale; // scale of orbiter circle in meters
    int basePoints; // geometric points of the circle, 3 = triangle, 4 = quad, etc.
    Vector3d rotation; // rotation vector in degrees
    Vector3d option; // Minecraft-defined range of randomness for particle
    double speed; // Minecraft-defined speed of particle
    int count; // Minecraft-defined count of particles per frame, 0 has special properties
    Shape geometry;

    boolean useSineWave = false; // controls whether to use sine wave
    double sineHeight; // scale of sine wave in meters
    int sineSpeed; // Speed of sine wave function in ticks
    int sineOffset; // offset of sine wave in ticks
    Vector3d sineRotation;
    InterpolationMode mode;
    boolean directionalOption;

    public ShapedOrbiter(String Name, ParticleEffect Particle, int Points, Shape Shape) {
        this.name = Name;
        this.particle = Particle;
        this.basePoints = Points;

        // set the unset, please edit with methods
        this.rotation = new Vector3d(0,0,0);
        this.option = new Vector3d(0,0,0);
        this.speed = 0;
        this.count = 1;
        this.circleScale = 1;
        this.geometry = Shape;

        this.useSineWave = false;
        this.sineHeight = 1;
        this.sineSpeed = 1;
        this.sineOffset = 0;
        this.sineRotation = new Vector3d(0,0,0);
        this.mode = null;
        this.directionalOption = false;

    }

    public ShapedOrbiter SetScale(double m) {
        this.circleScale = m;
        return this;
    }

    public ShapedOrbiter SetRotation(Vector3d r) {
        this.rotation = ToRadians(r);
        return this;
    }

    public ShapedOrbiter SetOffset(Vector3d o) {
        this.offset = o;
        return this;
    }

    public ShapedOrbiter SetOption(Vector3d d) {
        this.option = d;
        return this;
    }

    public ShapedOrbiter SetSpeed(double s) {
        this.speed = s;
        return this;
    }

    public ShapedOrbiter SetCount(int c) {
        this.count = c;
        return this;
    }

    public ShapedOrbiter SetDelay(int d) {
        this.delayTicks = d;
        return this;
    }

    public ShapedOrbiter SetReverse(boolean r) {
        this.reverse = r;
        return this;
    }

    public ShapedOrbiter SetVerticalSine(boolean s) {
        this.useSineWave = s;
        return this;
    }

    public ShapedOrbiter SetVSineHeight(double meters) {
        this.sineHeight = meters;
        return this;
    }

    public ShapedOrbiter SetVSineSpeed(int ticks) {
        this.sineSpeed = ticks;
        return this;
    }

    public ShapedOrbiter SetVSineOffset(int ticks) {
        this.sineOffset = ticks;
        return this;
    }

    public ShapedOrbiter SetVSineRotation(Vector3d r) {
        this.sineRotation = r;
        return this;
    }

    public ShapedOrbiter SetInterpolationMode(InterpolationMode m) {
        this.mode = m;
        return this;
    }

    public ShapedOrbiter SetFrameOffset(int f) {
        this.frameOffset = f;
        return this;
    }

    public ShapedOrbiter SetDirectionalOption(boolean d) {
        this.directionalOption = d;
        return this;
    }

    @Override
    public LoopedParticleEffect quickRender() {
        ArrayList<ParticlePrefab> frames = new ArrayList<>();
        ParticlePrefab current;
        Vector3d basePosition;
        Vector3d opt;
        int step = 1;
        int keyDistance = 1; // every x frames is keyframe, if using smoothInterp
        boolean usingInterp = false;
        boolean smoothInterp = false;

        int smoothKeyframe = 0;

        if (mode != null) {
            if (mode.isSmooth()) {
                basePoints *= mode.Subdivisions();
                sineSpeed *= mode.Subdivisions();
                smoothInterp = true;
            }
            keyDistance = mode.Subdivisions();
            usingInterp = true;
        }

        int totalPoints;
        if (useSineWave) {
            totalPoints = LCM(basePoints, sineSpeed);
        }
        else {
            totalPoints = basePoints;
        }

        for (int h = 0; h < totalPoints; h++) {

            // parse Option
            if (directionalOption) {
                opt = ApplyRotation(this.option, new Vector3d(0, Math.toRadians(-360 * (h / (double) basePoints)), 0));
                opt = ApplyRotation(opt, rotation);
            }
            else {
                opt = this.option;
            }

            // Make initial point from x,z circle
            basePosition = CircleGenerator(basePoints, FrameInRange(basePoints, step) - 1);

            // Apply rotation to x,z circle
            //basePosition = ApplyRotation(basePosition, rotation);

            // radius calculation (scale)
            basePosition.x *= circleScale;
            basePosition.y *= circleScale;
            basePosition.z *= circleScale;

            // Vertical sine wave stuff
            if (useSineWave) {
                Vector3d sine = SineOnY(sineSpeed, FrameInRange(sineSpeed, step + sineOffset) - 1, sineHeight);

                // Apply rotation to Sine effect
                sine = ApplyRotation(sine, sineRotation);

                // Add sine effect to position
                basePosition.x += sine.x;
                basePosition.y += sine.y;
                basePosition.z += sine.z;
            }

            // render points for shape
            boolean firstFrame = true;
            for (Vector3d pointPosition : this.geometry.GetGeometry()) {

                // Own transforms

                // apply own rotation offset
                pointPosition = ApplyRotation(pointPosition, this.geometry.GetRotation());
                // apply own scale
                pointPosition = new Vector3d(pointPosition.x * this.geometry.GetScale().x, pointPosition.y * this.geometry.GetScale().y, pointPosition.z * this.geometry.GetScale().z);
                // apply own offset
                pointPosition = new Vector3d(pointPosition.x + this.geometry.GetOffset().x, pointPosition.y + this.geometry.GetOffset().y, pointPosition.z + this.geometry.GetOffset().z);

                // base transforms

                // apply rotation depending where in animation things are
                pointPosition = ApplyRotation(pointPosition, new Vector3d(0, Math.toRadians(-360 * (h / (double) basePoints)), 0));

                // inherit base offset
                pointPosition = new Vector3d(pointPosition.x + basePosition.x, pointPosition.y + basePosition.y, pointPosition.z + basePosition.z);

                // inherit base rotation
                pointPosition = ApplyRotation(pointPosition, rotation);

                // inherit global rotation
                // apply global offset
                pointPosition.x += offset.x;
                pointPosition.y += offset.y;
                pointPosition.z += offset.z;

                // set primary step to be a keyframe or not
                // works for both smooth interp and no interp
                // this is combined with first point in shape

                if (smoothKeyframe == 0 && firstFrame) {
                    current = new ParticlePrefab(particle, pointPosition, opt, speed, count, true);
                    firstFrame = false;
                }
                else {
                    current = new ParticlePrefab(particle, pointPosition, opt, speed, count);
                }

                frames.add(current);
            }

            // logic for controlling whether or not to add keyframe for smooth interp
            if (smoothInterp) {
                if (smoothKeyframe == 0) smoothKeyframe = keyDistance - 1;
                else smoothKeyframe--;
            }

            // increment step for next frame calculations to proceed
            step++;
        }

        // implement linear interpolation after
        // having shapes means multiple points per frame, not making linear interp easy
        // working from the end and going backwards offers a way to not mutate the points I need to work on
        // this also allows me to add points as I go, not having to store them somewhere
        if (usingInterp && !smoothInterp) {
            int totalSize = frames.size();
            int FrameDistance = this.geometry.GetGeometry().size();
            int iterations = this.mode.Subdivisions();
            ArrayList<Vector3d>[] storedSubFrames = new ArrayList[totalSize];
            // work backwards, so we can use index as the index for ArrayList frames

            for (int index = totalSize; index > 0; index--) {
                Vector3d beginPosition = frames.get(index - 1).getPosition();
                Vector3d endPosition = frames.get(FrameInRange(totalSize, index + FrameDistance) - 1).getPosition();
                ArrayList<Vector3d> cache = new ArrayList<>();

                for (int subFrameIndex = iterations; subFrameIndex > 0; subFrameIndex--) {

                    Vector3d interpPosition = new Vector3d(
                            NumberInRange(subFrameIndex / (double) iterations, beginPosition.x, endPosition.x),
                            NumberInRange(subFrameIndex / (double) iterations, beginPosition.y, endPosition.y),
                            NumberInRange(subFrameIndex / (double) iterations, beginPosition.z, endPosition.z));

                    cache.add(interpPosition);
                }
                storedSubFrames[index - 1] = cache;
            }
            for (int index = totalSize; index > 0; index--) {
                ArrayList<Vector3d> activeCache = storedSubFrames[index - 1];
                for (Vector3d pos : activeCache) {
                    frames.add(index, new ParticlePrefab(particle, pos, option, speed, count));
                }
            }
        }

        base = new LoopedParticleEffect(name, frames, delayTicks, frameOffset, reverse);

        return base;
    }
}
