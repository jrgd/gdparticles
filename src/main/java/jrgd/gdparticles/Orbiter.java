package jrgd.gdparticles;

import jrgd.particlelib.Types.LoopedParticleEffect;
import jrgd.particlelib.Types.ParticlePrefab;
import jrgd.particlelib.Types.Vector3d;
import net.minecraft.particle.ParticleEffect;

import java.util.ArrayList;

import static jrgd.particlelib.Calculations.Rotation.*;
import static jrgd.particlelib.Calculations.Range.*;
import static jrgd.gdparticles.Function.LCM;

public class Orbiter extends LoopedParticleBase {

    // Add interpolation, old geometric and arc-based (subdivide CircleGenerator to make subpoints)

    double circleScale; // scale of orbiter circle in meters
    int points; // geometric points of the circle, 3 = triangle, 4 = quad, etc.
    Vector3d rotation; // rotation vector in degrees
    Vector3d option; // Minecraft-defined range of randomness for particle
    double speed; // Minecraft-defined speed of particle
    int count; // Minecraft-defined count of particles per frame, 0 has special properties

    boolean useSineWave = false; // controls whether to use sine wave
    double sineHeight; // scale of sine wave in meters
    int sineSpeed; // Speed of sine wave function in ticks
    int sineOffset; // offset of sine wave in ticks
    Vector3d sineRotation;
    InterpolationMode mode;
    boolean directionalOption;

    public Orbiter(String Name, ParticleEffect Particle, int Points) {
        this.name = Name;
        this.particle = Particle;
        this.points = Points;

        // set the unset, please edit with methods
        this.rotation = new Vector3d(0,0,0);
        this.option = new Vector3d(0,0,0);
        this.speed = 0;
        this.count = 1;
        this.circleScale = 1;

        this.useSineWave = false;
        this.sineHeight = 1;
        this.sineSpeed = 1;
        this.sineOffset = 0;
        this.sineRotation = new Vector3d(0,0,0);
        this.mode = null;
        this.directionalOption = false;

    }

    public Orbiter SetScale(double m) {
        this.circleScale = m;
        return this;
    }

    public Orbiter SetRotation(Vector3d r) {
        this.rotation = ToRadians(r);
        return this;
    }

    public Orbiter SetOffset(Vector3d o) {
        this.offset = o;
        return this;
    }

    public Orbiter SetOption(Vector3d d) {
        this.option = d;
        return this;
    }

    public Orbiter SetSpeed(double s) {
        this.speed = s;
        return this;
    }

    public Orbiter SetCount(int c) {
        this.count = c;
        return this;
    }

    public Orbiter SetDelay(int d) {
        this.delayTicks = d;
        return this;
    }

    public Orbiter SetReverse(boolean r) {
        this.reverse = r;
        return this;
    }

    public Orbiter SetVerticalSine(boolean s) {
        this.useSineWave = s;
        return this;
    }

    public Orbiter SetVSineHeight(double meters) {
        this.sineHeight = meters;
        return this;
    }

    public Orbiter SetVSineSpeed(int ticks) {
        this.sineSpeed = ticks;
        return this;
    }

    public Orbiter SetVSineOffset(int ticks) {
        this.sineOffset = ticks;
        return this;
    }

    public Orbiter SetVSineRotation(Vector3d r) {
        this.sineRotation = r;
        return this;
    }

    public Orbiter SetInterpolationMode(InterpolationMode m) {
        this.mode = m;
        return this;
    }

    public Orbiter SetFrameOffset(int f) {
        this.frameOffset = f;
        return this;
    }

    public Orbiter SetDirectionalOption(boolean d) {
        this.directionalOption = d;
        return this;
    }

    @Override
    public LoopedParticleEffect quickRender() {
        ArrayList<ParticlePrefab> frames = new ArrayList<>();
        ParticlePrefab current = null;
        ParticlePrefab previous;
        Vector3d pos;
        Vector3d opt;
        int step = 1;
        int keyDistance = 1; // every x frames is keyframe, if using smoothInterp
        boolean usingInterp = false;
        boolean smoothInterp = false;

        int smoothKeyframe = 0;



        if (mode != null) {
            if (mode.isSmooth()) {
                points *= mode.Subdivisions();
                sineSpeed *= mode.Subdivisions();
                smoothInterp = true;
            }
            keyDistance = mode.Subdivisions();
            usingInterp = true;
        }

        int totalPoints;
        if (useSineWave) {
            totalPoints = LCM(points, sineSpeed);
        }
        else {
            totalPoints = points;
        }


        for (int h = 0; h < totalPoints; h++) {

            // parse Option
            if (directionalOption) {
                opt = ApplyRotation(this.option, new Vector3d(0, Math.toRadians(-360 * (h / (double) points)), 0));
                opt = ApplyRotation(opt, rotation);
            }
            else {
                opt = this.option;
            }

            // keyframe storage for linear interpolation
            // linear interpolation needs to know of two frames
            // this is why this is necessary
            previous = current;

            // Make initial point from x,z circle
            pos = CircleGenerator(points, FrameInRange(points, step) - 1);

            // Apply rotation to x,z circle
            pos = ApplyRotation(pos, rotation);

            // radius calculation (scale)
            pos.x *= circleScale;
            pos.y *= circleScale;
            pos.z *= circleScale;

            // Vertical sine wave stuff
            if (useSineWave) {
                Vector3d sine = SineOnY(sineSpeed, FrameInRange(sineSpeed, step + sineOffset) - 1, sineHeight);

                // Apply rotation to Sine effect
                sine = ApplyRotation(sine, sineRotation);

                // Add sine effect to position
                pos.x += sine.x;
                pos.y += sine.y;
                pos.z += sine.z;
            }

            // apply global offset
            pos.x += offset.x;
            pos.y += offset.y;
            pos.z += offset.z;

            // set primary step to be a keyframe or not
            // works for both smooth interp and no interp

            if (smoothKeyframe == 0)
                current = new ParticlePrefab(particle, pos, opt, speed, count, true);
            else
                current = new ParticlePrefab(particle, pos, opt, speed, count);


            // subdivision frame rendering for linear interp
            if (usingInterp && !smoothInterp && previous != null ) {
                Vector3d currentPos = current.getPosition();
                Vector3d lastPos = previous.getPosition();

                // distance scaling for linear raw frames (subkeyframes)
                for (int f = 1; f < keyDistance; f++) {
                    Vector3d interpPos = new Vector3d(
                        NumberInRange(((double) f / (double) keyDistance), lastPos.x, currentPos.x),
                        NumberInRange(((double) f / (double) keyDistance), lastPos.y, currentPos.y),
                        NumberInRange(((double) f / (double) keyDistance), lastPos.z, currentPos.z));

                    // add subdivision frame to list of frames

                    frames.add(new ParticlePrefab(particle, interpPos, option, speed, count));
                }
            }

            // add main frame rendered, be it keyframe or smooth interp subdiv frame
            // must be placed after linear interp
            // frames are calculated from current to previous,
            // therefore wedging frames in-between is the optimal solution
            frames.add(current);

            // logic for controlling whether or not to add keyframe for smooth interp
            if (smoothInterp) {
                if (smoothKeyframe == 0) smoothKeyframe = keyDistance - 1;
                else smoothKeyframe--;
            }

            // increment step for next frame calculations to proceed
            step++;
        }

        if (usingInterp && !smoothInterp) {
            Vector3d currentPos = frames.get(0).getPosition();
            Vector3d lastPos = current.getPosition();

            // distance scaling for linear raw frames (subkeyframes)
            for (int f = 1; f < keyDistance; f++) {
                Vector3d interpPos = new Vector3d(
                        NumberInRange(((double) f / (double) keyDistance), lastPos.x, currentPos.x),
                        NumberInRange(((double) f / (double) keyDistance), lastPos.y, currentPos.y),
                        NumberInRange(((double) f / (double) keyDistance), lastPos.z, currentPos.z));

                // add subdivision frame to list of frames

                frames.add(new ParticlePrefab(particle, interpPos, option, speed, count));
            }
        }

        base = new LoopedParticleEffect(name, frames, delayTicks, frameOffset, reverse);

        return base;
    }
}
