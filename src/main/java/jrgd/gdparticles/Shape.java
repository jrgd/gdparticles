package jrgd.gdparticles;

import jrgd.particlelib.Types.Vector3d;

import java.util.ArrayList;

import static jrgd.particlelib.Calculations.Rotation.*;

public class Shape {
    private ArrayList<Vector3d> points;
    private Vector3d rotation;
    private Vector3d scale;
    private Vector3d offset;

    public Shape(ArrayList<Vector3d> Points) {
        this.points = Points;
        this.rotation = new Vector3d(0,0,0);
        this.scale = new Vector3d(1,1,1);
        this.offset = new Vector3d(0 ,0,0);
    }
    public Shape SetRotation(Vector3d Rotation) {
        this.rotation = ToRadians(Rotation);
        return this;
    }

    public Shape SetScale(Vector3d Scale) {
        this.scale = Scale;
        return this;
    }

    public Shape SetOffset(Vector3d Offset) {
        this.offset = Offset;
        return this;
    }

    public ArrayList<Vector3d> GetGeometry() {
        return this.points;
    }

    public Vector3d GetRotation() {
        return this.rotation;
    }

    public Vector3d GetScale() {
        return this.scale;
    }

    public Vector3d GetOffset() {
        return this.offset;
    }
}
