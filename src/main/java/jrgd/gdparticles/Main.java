package jrgd.gdparticles;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.registry.CommandRegistry;

public class Main implements ModInitializer {

	@Override
	public void onInitialize() {

		CommandRegistry.INSTANCE.register(false, Commands::register);

	}

}
